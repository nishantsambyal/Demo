package com.example.nishant.demo1.view;

/**
 * Created by Nishant on 12/28/2017.
 */

public interface LoginView {

    void loginValidation();
    void loginSuccess();
    void loginError();
}
